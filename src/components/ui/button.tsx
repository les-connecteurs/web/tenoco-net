import React from 'react';

import * as colors from '../colors';

type CommonProps = {
  color: colors.Color;
  outline?: boolean;
  className?: string;
};

type ButtonProps = CommonProps & React.ButtonHTMLAttributes<HTMLButtonElement>;

const generateClassName = ({
  color,
  outline,
  className,
}: CommonProps): string => {
  const style = outline
    ? `${colors.outline(color)}`
    : `${colors.outline(color)} ${colors.fill(color)}`;
  return `border-2 px-4 py-1 rounded leading-8 uppercase inline-block font-body font-semibold ${style} ${className}`;
};

const Button: React.RefForwardingComponent<HTMLButtonElement, ButtonProps> = (
  { color, outline, children, className, ...props }: ButtonProps,
  ref: React.Ref<HTMLButtonElement>,
) => (
  <button
    ref={ref}
    className={generateClassName({ color, outline, className })}
    {...props}
  >
    {children}
  </button>
);

type LinkProps = CommonProps & React.AnchorHTMLAttributes<HTMLAnchorElement>;

const Link: React.RefForwardingComponent<HTMLAnchorElement, LinkProps> = (
  { color, outline, children, className, ...props }: LinkProps,
  ref: React.Ref<HTMLAnchorElement>,
) => (
  <a
    ref={ref}
    className={generateClassName({ color, outline, className })}
    {...props}
  >
    {children}
  </a>
);

const FButton = React.forwardRef<HTMLButtonElement, ButtonProps>(Button);
const FLink = React.forwardRef<HTMLAnchorElement, LinkProps>(Link);

export default FButton;
export { FLink as LinkButton, FButton as Button };
