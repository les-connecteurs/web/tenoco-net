import React from 'react';

import { outline } from '../colors';

type InputProps = React.HTMLProps<HTMLInputElement>;

const Input: React.RefForwardingComponent<HTMLInputElement, InputProps> = (
  { className, ...props }: InputProps,
  ref: React.Ref<HTMLInputElement>,
) => {
  return (
    <input
      ref={ref}
      className={`border-2 p-2 rounded-sm ${outline(
        'teal',
        'dark-green',
      )} ${className}`}
      {...props}
    />
  );
};

export default React.forwardRef<HTMLInputElement, InputProps>(Input);
