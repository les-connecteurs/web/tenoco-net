import React from 'react';

import * as colors from '../colors';

type ServiceProps = {
  className?: string;
  title: string;
  subtitle: string;
  icon: string;
  href: string;
} & (
  | { active?: false; color?: colors.Color }
  | { active: true; color: colors.Color }
);

const Service: React.RefForwardingComponent<HTMLAnchorElement, ServiceProps> = (
  { className, title, subtitle, icon, href, ...props }: ServiceProps,
  ref: React.Ref<HTMLAnchorElement>,
) => {
  const style = props.active ? colors.fill(props.color) : '';
  return (
    <a
      ref={ref}
      href={href}
      className={`${style} rounded px-2 py-4 flex items-center ${
        className || ''
      }`}
    >
      <div className={`fa ${icon} text-5xl text-center w-16 mr-4`}></div>
      <div className="flex-1 leading-tight">
        <div className="font-display font-medium text-2xl">{title}</div>
        <div className="font-body hidden lg:block">{subtitle}</div>
      </div>
    </a>
  );
};

export default React.forwardRef<HTMLAnchorElement, ServiceProps>(Service);
