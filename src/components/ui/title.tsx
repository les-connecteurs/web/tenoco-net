import React from 'react';

import { Color, fill } from '../colors';

type Level = 1 | 2 | 3;

type TitleProps = {
  level: Level;
  children: React.ReactNode;
  underline: Color;
};

const elemMap: Record<Level, string> = {
  1: 'h1',
  2: 'h2',
  3: 'h3',
};

const classMap: Record<Level, string> = {
  1: 'text-5xl',
  2: 'text-3xl',
  3: 'text-xl',
};

const split = (children: React.ReactNode): Array<React.ReactChild> => {
  if (typeof children === 'string') {
    return children.split(' ');
  } else if (typeof children === 'number' || typeof children === 'boolean') {
    return [children.toString()];
  } else if (Array.isArray(children)) {
    return children.flatMap(split);
  } else if (children === null) {
    return [];
  } else if (React.isValidElement(children)) {
    return [children];
  } else {
    return [];
  }
};

const Title: React.FC<TitleProps> = ({
  level,
  children,
  underline,
}: TitleProps) => {
  const list = split(children);
  return React.createElement(
    elemMap[level],
    {
      className: `heading font-display font-medium flex flex-wrap mb-2 ${classMap[level]}`,
    },
    list.map((node, index) => {
      return (
        <span key={index} className="heading-underline">
          {node}
          <span className={`heading-underline-block ${fill(underline)}`}>
            {' '}
          </span>
        </span>
      );
    }),
  );
};

export default Title;
