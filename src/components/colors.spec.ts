import { spec, list } from './colors';

test('color variants', () => {
  for (const color of list) {
    const s = spec(color);
    expect(s.background).toEqual(`bg-${color}`);
    expect(s.outline).toEqual(`border-${color}`);
    expect(s.focus).toEqual(`focus:border-${color}`);
    expect(s.text).toEqual(`text-${color}`);
    expect(s.contrast).not.toEqual(color);
  }
});
