import React from 'react';

import { fill, Color } from '../colors';

type SectionDirection = 'up' | 'down';

type SectionProps = {
  color: Color;
  direction: SectionDirection;
  children?: React.ReactNode;
} & React.HTMLProps<HTMLElement>;

const Section: React.FC<SectionProps> = ({
  color,
  direction,
  children,
  className,
  ...props
}: SectionProps) => (
  <section
    {...props}
    className={[
      fill(color),
      className,
      direction === 'up' ? 'section-up' : 'section-down',
    ]
      .filter(Boolean)
      .join(' ')}
  >
    {children}
  </section>
);

const Spacer: React.FC = () => <div className="section-spacer" />;

export { Spacer };

export default Section;
