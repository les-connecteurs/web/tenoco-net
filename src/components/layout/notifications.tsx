import React from 'react';

import { useErrors, usePopError, AppError } from '../../error-manager';
import { Button } from '../ui/button';

type NotificationProps = {
  error: AppError;
  onClose: () => void;
};
const Notification: React.FC<NotificationProps> = ({
  error,
  onClose,
}: NotificationProps) => (
  <div className="bg-red p-4 mb-4 text-dark font-semibold items-center flex">
    <p className="mx-4">{error.message}</p>
    <Button color="dark" outline onClick={onClose}>
      Close
    </Button>
  </div>
);

const Notifications: React.FC = () => {
  const errors = useErrors();
  const popError = usePopError();

  // TODO: keys
  return (
    <div className="fixed bottom-0 right-0 flex flex-col pr-4">
      {errors.map((error, index) => (
        <Notification
          key={index}
          error={error}
          onClose={() => popError(error)}
        />
      ))}
    </div>
  );
};

export default Notifications;
