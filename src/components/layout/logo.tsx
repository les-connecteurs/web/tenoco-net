import React from 'react';

const Logo: React.FC = () => (
  <h1 className="font-body font-bold text-4xl uppercase logo">Tenoco</h1>
);

export default Logo;
