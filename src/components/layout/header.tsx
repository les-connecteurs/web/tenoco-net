import React from 'react';

import Logo from './logo';
// import Menu from './menu';

const Header: React.FC = () => (
  <header className="container mx-auto flex items-center justify-between p-4">
    <Logo />
    {/* <Menu /> */}
  </header>
);

export default Header;
