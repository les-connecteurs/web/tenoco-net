import React from 'react';

type MenuItemProps = {
  children: React.ReactNode;
};

const MenuItem: React.FC<MenuItemProps> = ({ children }: MenuItemProps) => (
  <li className="font-body uppercase px-2 font-semibold">{children}</li>
);

const items = ['Item 1', 'Item 2', 'Item 3'];

const Menu: React.FC = () => (
  <ul className="flex">
    {items.map((item, index) => (
      <MenuItem key={index}>{item}</MenuItem>
    ))}
  </ul>
);

export default Menu;
