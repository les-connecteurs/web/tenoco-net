export type Color = 'light' | 'dark-green' | 'dark' | 'yellow' | 'red' | 'teal';

export const list: Color[] = [
  'light',
  'dark-green',
  'dark',
  'yellow',
  'red',
  'teal',
];

type ColorSpec = {
  background: string;
  outline: string;
  focus: string;
  text: string;
  contrast: Color;
};

const colorMap: Record<Color, ColorSpec> = {
  light: {
    background: 'bg-light',
    outline: 'border-light',
    focus: 'focus:border-light',
    text: 'text-light',
    contrast: 'dark',
  },
  dark: {
    background: 'bg-dark',
    outline: 'border-dark',
    focus: 'focus:border-dark',
    text: 'text-dark',
    contrast: 'light',
  },
  'dark-green': {
    background: 'bg-dark-green',
    outline: 'border-dark-green',
    focus: 'focus:border-dark-green',
    text: 'text-dark-green',
    contrast: 'light',
  },
  yellow: {
    background: 'bg-yellow',
    outline: 'border-yellow',
    focus: 'focus:border-yellow',
    text: 'text-yellow',
    contrast: 'dark',
  },
  red: {
    background: 'bg-red',
    outline: 'border-red',
    focus: 'focus:border-red',
    text: 'text-red',
    contrast: 'dark',
  },
  teal: {
    background: 'bg-teal',
    outline: 'border-teal',
    focus: 'focus:border-teal',
    text: 'text-teal',
    contrast: 'dark',
  },
};

const spec = (color: Color): ColorSpec => colorMap[color];

const _text = (color: Color): string => spec(color).text;
const _outline = (color: Color): string => spec(color).outline;
const _focus = (color: Color): string => spec(color).focus;
const _background = (color: Color): string => spec(color).background;

const _contrastFallback = (color: Color, contrastOverride?: Color): Color =>
  contrastOverride || contrast(color);

const contrast = (color: Color): Color => spec(color).contrast;

const fill = (color: Color, contrastOverride?: Color): string =>
  color
    ? [
        _text(_contrastFallback(color, contrastOverride)),
        _background(color),
      ].join(' ')
    : '';

const outline = (color: Color, contrastOverride?: Color): string =>
  color
    ? [
        _outline(color),
        _focus(_contrastFallback(color, contrastOverride)),
        'focus:outline-none',
      ].join(' ')
    : '';

export { fill, outline, contrast, spec };
