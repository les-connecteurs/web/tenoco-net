import React, { createContext, useReducer, useContext } from 'react';

type ChangeError = (error: AppError) => void;
type ErrorContext = {
  errors: AppError[];
  push?: ChangeError;
  pop?: ChangeError;
};

export type AppError = {
  message: string;
};

const ErrorContext = createContext<ErrorContext>({ errors: [] });

type ActionType = 'push' | 'pop';
type Action = {
  type: ActionType;
  error: AppError;
};

function reducer(state: AppError[], action: Action): AppError[] {
  switch (action.type) {
    case 'push':
      return [...state, action.error];
    case 'pop':
      return state.filter((e) => e !== action.error);
  }
}

type ErrorManagerProps = {
  defaultErrors: AppError[];
  children: React.ReactNode;
};

export const ErrorManager: React.FC<ErrorManagerProps> = ({
  defaultErrors,
  children,
}: ErrorManagerProps) => {
  const [errors, dispatch] = useReducer(reducer, defaultErrors);

  const push = (error: AppError) => dispatch({ type: 'push', error });
  const pop = (error: AppError) => dispatch({ type: 'pop', error });

  return (
    <ErrorContext.Provider value={{ errors, push, pop }}>
      {children}
    </ErrorContext.Provider>
  );
};

export const useErrors = (): AppError[] => useContext(ErrorContext).errors;
export const usePushError = (): ChangeError =>
  useContext(ErrorContext).push ||
  ((err: AppError) => {
    // That should not happen, but just in case, log the error
    console.error(err);
  });

export const usePopError = (): ChangeError =>
  useContext(ErrorContext).pop ||
  (() => {
    throw new Error();
  });
