import type { Resource } from 'i18next';
import type { AppError } from './error-manager';

export type ClientData = {
  i18n: Resource;
  errors: AppError[];
};
