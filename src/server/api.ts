import { Router } from 'express';
import { json, urlencoded } from 'body-parser';

import { save } from './subscription';

const app = Router();

app.use(urlencoded({ extended: false }));
app.use(json());

app.post('/interested', async (req, res) => {
  try {
    if (typeof req.body.email !== 'string') {
      throw new Error('invalid or missing email property');
    }

    await save({
      email: req.body.email,
      userAgent: req.get('User-Agent'),
      ipAddress: req.ip,
      lang: req.i18n.language,
    });

    res.json({ status: 'ok' });
  } catch (e) {
    res.status(400);
    res.json({ error: e.message });
  }
});

export default app;
