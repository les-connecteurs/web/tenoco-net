import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import i18nextMiddleware from '@sandhose/i18next-http-middleware';
import getPort from 'get-port';
import { urlencoded } from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import { v4 as uuidv4 } from 'uuid';

import type { AppError } from '../error-manager';
import type { ClientData } from '../data';
import i18n from './i18n';
import html from './html';
import { render } from './render';
import wellKnown from './well-known';
import api from './api';
import { save } from './subscription';

const server = express();

const secure = process.env.NODE_ENV === 'production';

server.set('trust proxy', 'loopback');

server.use((req, res, next) => {
  res.locals.nonce = uuidv4();
  next();
});

server.use(
  helmet({
    frameguard: { action: 'deny' },
    contentSecurityPolicy: {
      directives: {
        baseUri: ["'none'"],
        defaultSrc: ["'self'"],
        imgSrc: ["'self'"],
        scriptSrc: ["'self'", (req, res) => `'nonce-${res.locals.nonce}'`],
        styleSrc: ["'self'"],
        fontSrc: ["'self'"],
        formAction: ["'self'"],
        connectSrc: ["'self'"], // For fetching locales

        frameSrc: ["'none'"],
        manifestSrc: ["'none'"],
        mediaSrc: ["'none'"],
        objectSrc: ["'none'"],
        workerSrc: ["'none'"],

        frameAncestors: ["'none'"],
        sandbox: ['allow-scripts', 'allow-forms', 'allow-same-origin'],
        upgradeInsecureRequests: true,
        blockAllMixedContent: true,
      },
    },
    referrerPolicy: {
      policy: 'no-referrer',
    },
    expectCt: secure && {
      enforce: true,
      maxAge: 365 * 24 * 60 * 60,
    },
    hsts: secure && {
      maxAge: 365 * 24 * 60 * 60,
      includeSubDomains: true,
    },
  }),
);

server.use(morgan('combined'));
server.use(
  i18nextMiddleware.handle(i18n, {
    ignoredRoutes: ['/locales', '/_dist_', '/web_modules'],
  }),
);

server.use(compression());

server.use('/.well-known', wellKnown);
server.use('/api', api);

server.post('/locales/add/:lng/:ns', i18nextMiddleware.missingKeyHandler(i18n));
server.get(
  '/locales/resources.json',
  i18nextMiddleware.getResourcesHandler(i18n),
);

server.post('/', urlencoded({ extended: false }), async (req, res, next) => {
  const errors: AppError[] = [];
  const email = req.body.email;

  if (typeof email !== 'string') {
    errors.push({
      message: 'missing email field',
    });
  } else {
    try {
      await save({
        email,
        userAgent: req.get('User-Agent'),
        ipAddress: req.ip,
        lang: req.i18n.language,
      });
    } catch (e) {
      errors.push({
        message: e.message,
      });
    }
  }

  try {
    const body = await render(req.i18n, errors);
    const data: ClientData = {
      i18n: req.i18n.store.data,
      errors,
    };

    res.send(
      await html({ body, data, nonce: res.locals.nonce, i18n: req.i18n }),
    );
  } catch (error) {
    next(error);
  }
});

server.get('/', async (req, res, next) => {
  try {
    const body = await render(req.i18n, []);
    const data = {
      i18n: req.i18n.store.data,
      errors: [],
    };

    res.send(
      await html({ body, data, nonce: res.locals.nonce, i18n: req.i18n }),
    );
  } catch (error) {
    next(error);
  }
});

// Kinda ugly but it works.
// TODO: proper font handling
server.use('/fonts', cors(), express.static('node_modules/fork-awesome/fonts'));
server.use(cors(), express.static('build'));

getPort({
  port: process.env.PORT ? parseInt(process.env.PORT) : 3042,
}).then((port) =>
  server.listen(port, () => console.log(`server listening on port ${port}`)),
);
