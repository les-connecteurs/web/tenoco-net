import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import { LanguageDetector } from '@sandhose/i18next-http-middleware';
import Backend from '@sandhose/i18next-fs-backend';

const i18n = i18next.createInstance();

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: false,
    lng: 'fr',
    fallbackLng: 'fr',
    supportedLngs: ['fr', 'en'],
    preload: ['fr', 'en'],
    ns: ['home', 'translation'],
    defaultNS: 'translation',
    saveMissing: true,

    backend: {
      loadPath: 'locales/{{lng}}/{{ns}}.json',
      addPath: 'locales/{{lng}}/{{ns}}.missing.json',
    },
  });

i18n.loadLanguages('fr');
i18n.loadNamespaces('translation');

export default i18n;
