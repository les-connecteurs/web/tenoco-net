import { Router } from 'express';
import cors from 'cors';

const app = Router();

app.use(cors());

app.get('/matrix/client', (req, res) =>
  res.json({
    'm.homeserver': {
      base_url: 'https://matrix.tenoco.net/',
    },
    'm.identity_server': {
      base_url: 'https://matrix.org',
    },
  }),
);

app.get('/matrix/server', (req, res) =>
  res.json({
    'm.server': 'matrix.tenoco.net:443',
  }),
);

app.all('/jmap', (req, res) => res.redirect('https://jmap.tenoco.net/jmap/'));

export default app;
