import ReactDOMServer from 'react-dom/server';
import React from 'react';
import serialize from 'serialize-javascript';
import type * as i18next from 'i18next';
import { createReadStream } from 'fs';
import { createHash } from 'crypto';

import type { ClientData } from '../data';

// List of modules to preload
const modules = [
  'i18next.js',
  'i18next-browser-languagedetector.js',
  'i18next-fetch-backend.js',
  'react.js',
  'react-dom.js',
  'react-i18next.js',
];

const sriCache: Record<string, string> = {};

function fileIntegrity(hashName: string, path: string): Promise<string> {
  return new Promise((resolve, reject) => {
    const hash = createHash(hashName);
    hash.setEncoding('base64');
    const stream = createReadStream(path);
    stream.on('error', (err) => reject(err));
    stream.on('data', (chunk) => hash.update(chunk));
    stream.on('end', () => {
      hash.end();
      resolve(`${hashName}-${hash.read()}`);
    });
  });
}

const sri = async (path: string): Promise<string> => {
  if (!sriCache[path]) {
    sriCache[path] = await fileIntegrity('sha256', `build${path}`);
  }

  return sriCache[path];
};

type IconSpec = {
  sizes: string;
  generic: boolean;
  android: boolean;
  ios: boolean;
  integrity: string;
  path: string;
};

const icons = async (): Promise<React.ReactNode[]> => {
  const list = [
    { size: 32, generic: true },
    { size: 128, generic: true },
    { size: 152, ios: true },
    { size: 167, ios: true },
    { size: 180, ios: true },
    { size: 192, generic: true },
    { size: 196, android: true },
  ];

  const specs: IconSpec[] = await Promise.all(
    list.map(async (i) => ({
      sizes: `${i.size}x${i.size}`,
      generic: i.generic || false,
      android: i.android || false,
      ios: i.ios || false,
      path: `/favicon-${i.size}.png`,
      integrity: await sri(`/favicon-${i.size}.png`),
    })),
  );

  return [
    ...specs
      .filter((x) => x.generic)
      .map(({ sizes, path, integrity }) => (
        <link
          key={`generic-${sizes}`}
          rel="icon"
          crossOrigin="anonymous"
          href={path}
          sizes={sizes}
          integrity={integrity}
        />
      )),
    ...specs
      .filter((x) => x.ios)
      .map(({ sizes, path, integrity }) => (
        <link
          key={`ios-${sizes}`}
          rel="apple-touch-icon"
          crossOrigin="anonymous"
          href={path}
          sizes={sizes}
          integrity={integrity}
        />
      )),
    ...specs
      .filter((x) => x.android)
      .map(({ sizes, path, integrity }) => (
        <link
          key={`android-${sizes}`}
          rel="shortcut icon"
          crossOrigin="anonymous"
          href={path}
          sizes={sizes}
          integrity={integrity}
        />
      )),
  ];
};

const html = async ({
  body,
  nonce,
  data,
  i18n,
}: {
  body: string;
  nonce: string;
  data: ClientData;
  i18n: i18next.i18n;
}): Promise<string> => {
  const script = `window.__CLIENT_DATA__=${serialize(data)};`;
  const doc = (
    <html lang={i18n.languages[0]}>
      <head>
        <meta charSet="utf-8" />
        <link
          rel="icon"
          href="/favicon.ico"
          crossOrigin="anonymous"
          integrity={await sri('/favicon.ico')}
        />
        {await icons()}
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Tenoco.net</title>
        <link rel="modulepreload" href="/_dist_/App.tsx" />
        {modules.map((m) => (
          <link rel="modulepreload" href={`/web_modules/${m}`} key={m} />
        ))}
        <link
          rel="stylesheet"
          href="/_dist_/index.css"
          crossOrigin="anonymous"
          integrity={await sri('/_dist_/index.css')}
        />
      </head>
      <body>
        <div id="root" dangerouslySetInnerHTML={{ __html: body }}></div>
        <script
          type="text/javascript"
          nonce={nonce}
          dangerouslySetInnerHTML={{ __html: script }}
        />
        <script
          type="importmap"
          src="/web_modules/import-map.json"
          crossOrigin="anonymous"
          integrity={await sri('/web_modules/import-map.json')}
        ></script>
        <script
          async
          type="module"
          src="/_dist_/client/index.js"
          crossOrigin="anonymous"
          integrity={await sri('/_dist_/client/index.js')}
        ></script>
      </body>
    </html>
  );
  return `<!DOCTYPE html>${ReactDOMServer.renderToStaticMarkup(doc)}`;
};

export default html;
