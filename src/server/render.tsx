import ReactDOMServer from 'react-dom/server';
import React from 'react';
import type { i18n } from 'i18next';
import { I18nextProvider, withSSR } from 'react-i18next';

import App from '../App';
import { ErrorManager, AppError } from '../error-manager';

const ExtendedApp = withSSR()(App);

function streamToString(stream: NodeJS.ReadableStream): Promise<string> {
  const chunks: Buffer[] = [];
  return new Promise((resolve, reject) => {
    stream.on('data', (chunk) => chunks.push(chunk));
    stream.on('error', reject);
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  });
}

const app = (i: i18n, errors: AppError[]): React.ReactElement => (
  <React.StrictMode>
    <I18nextProvider i18n={i}>
      <React.Suspense fallback="Loading…">
        <ErrorManager defaultErrors={errors}>
          <ExtendedApp
            initialI18nStore={i.store.data}
            initialLanguage={i.language}
          />
        </ErrorManager>
      </React.Suspense>
    </I18nextProvider>
  </React.StrictMode>
);

const render = async (i: i18n, errors: AppError[]): Promise<string> => {
  const stream = ReactDOMServer.renderToNodeStream(app(i, errors));
  return await streamToString(stream);
};

export { render };
