import { validate } from 'email-validator';

import knex from './knex';

type Subscription = {
  email: string;
  userAgent?: string;
  ipAddress?: string;
  lang?: string;
};

export const save = async (sub: Subscription): Promise<void> => {
  if (!validate(sub.email)) {
    throw new Error('invalid email address');
  }

  await knex('subscriptions').insert({
    email: sub.email,
    user_agent: sub.userAgent,
    ip: sub.ipAddress,
    lang: sub.lang,
  });
};
