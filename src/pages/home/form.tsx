import React, {
  forwardRef,
  useRef,
  useImperativeHandle,
  useState,
} from 'react';

import Input from '../../components/ui/input';
import Button from '../../components/ui/button';

type FormProps = {
  onSubmit: (email: string) => Promise<void>;
  onError?: (err: Error) => void;
};

export type FormRef = {
  focus: () => void;
};

const Form: React.RefForwardingComponent<FormRef, FormProps> = (
  { onSubmit, onError }: FormProps,
  ref: React.Ref<FormRef>,
) => {
  const inputRef: React.Ref<HTMLInputElement> = useRef(null);
  const [loading, setLoading] = useState(false);

  const submitHandler: React.FormEventHandler = async (e) => {
    e.preventDefault();
    if (loading) return;
    if (!inputRef.current) return;

    setLoading(true);
    try {
      await onSubmit(inputRef.current.value);
      inputRef.current.value = ''; // empty the field
    } catch (e) {
      if (onError) onError(e);
    }
    setLoading(false);
  };

  useImperativeHandle(ref, () => ({
    focus: () => {
      inputRef.current?.focus();
    },
  }));

  return (
    <form
      action="/"
      method="POST"
      id="form"
      onSubmit={submitHandler}
      className="w-64 my-4 mx-auto min-h-half-screen flex flex-col justify-center"
    >
      <h3 className="text-xl font-display font-medium mb-2">
        Tenez-moi au courant
      </h3>
      <Input
        className="mb-2"
        type="email"
        name="email"
        ref={inputRef}
        disabled={loading}
        placeholder="jean.dupont@courriel.fr"
      />
      <Button color="teal" disabled={loading} type="submit">
        Connectez-moi
      </Button>
    </form>
  );
};

export default forwardRef(Form);
