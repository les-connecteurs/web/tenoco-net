import React from 'react';
import { useTranslation } from 'react-i18next';
import { Trans } from 'react-i18next';

import { LinkButton } from '../../components/ui/button';

type LinkProps = {
  children: React.ReactNode;
} & React.HTMLProps<HTMLAnchorElement>;

const Link: React.FC<LinkProps> = ({ children, ...props }: LinkProps) => (
  <a className="text-red font-medium hover:underline" {...props}>
    {children}
  </a>
);

type HeroProps = {
  onConnect: () => void;
};

const Hero: React.FC<HeroProps> = ({ onConnect }: HeroProps) => {
  const { t } = useTranslation('home');
  return (
    <section className="container mx-auto flex justify-around items-center text-dark">
      <div className="w-96 my-8">
        <h2 className="text-2xl font-medium font-display mb-2">
          {t('hero.headline')}
        </h2>
        <p className="mb-4">
          <Trans t={t} i18nKey="hero.text">
            Nous construisons <em>votre</em> plateforme regroupant vos{' '}
            <Link href="#service-mail">courriers électroniques</Link>, votre{' '}
            <Link href="#service-chat">messagerie instantanée</Link> et votre{' '}
            <Link href="#service-social">flux social d&apos;activité</Link>.
          </Trans>
        </p>
        <LinkButton color="yellow" href="#form" onClick={onConnect}>
          {t('hero.call-to-action')}
        </LinkButton>
      </div>
    </section>
  );
};

export default Hero;
