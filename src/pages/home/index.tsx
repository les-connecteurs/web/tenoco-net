import React, { useRef } from 'react';

import type { Color } from '../../components/colors';
import Title from '../../components/ui/title';
import Header from '../../components/layout/header';
import Section, { Spacer } from '../../components/layout/section';
import { usePushError } from '../../error-manager';

import Form, { FormRef } from './form';
import ServiceList, { ServiceAnchor } from './service-list';
import Hero from './hero';

type HomeSectionProps = {
  background: Color;
  highlight: Color;
  id: ServiceAnchor;
  direction: 'up' | 'down';
  children: React.ReactNode;
};

const HomeSection: React.FC<HomeSectionProps> = ({
  background,
  highlight,
  id,
  children,
  direction,
}: HomeSectionProps) => {
  return (
    <Section id={id} color={background} direction={direction}>
      <div
        className={`container mx-auto flex p-2 items-center ${
          direction === 'up' ? 'flex-row-reverse' : 'flex-row'
        }`}
      >
        <ServiceList
          active={id}
          color={highlight}
          className="w-64 lg:w-96 hidden md:block"
        />
        {children}
      </div>
    </Section>
  );
};

const Home: React.FC = () => {
  const pushError = usePushError();
  const formRef: React.Ref<FormRef> = useRef(null);
  const onConnect = () => {
    // Delay the focus to the next tick
    setTimeout(() => {
      formRef.current?.focus();
    });
  };

  const onSubmit = async (email: string) => {
    const res = await fetch('/api/interested', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({ email }),
    });

    if (res.status >= 400) {
      throw new Error((await res.json()).error);
    }

    const data = await res.json();
    console.log(data);
  };

  const onError = (err: Error) => {
    pushError({
      message: err.message,
    });
  };

  return (
    <>
      <Header />
      <Hero onConnect={onConnect} />
      <Spacer />
      <Section color="teal" direction="up">
        <div className="max-w-3xl mx-auto my-8">
          <div className="flex items-stretch">
            <div className="flex-1 p-2 my-2">
              <Title level={2} underline="yellow">
                Des services qui vous respectent
              </Title>

              <p className="font-body mt-2">
                Reprenez le contrôle de votre vie privée.
              </p>
              <p className="font-body mb-2">
                Cette plateforme n’est{' '}
                <strong>pas financée par la publicité</strong>. Nous ne vivons
                pas de la revente ni de l’exploitation de vos données
                personnelles.
              </p>
            </div>
            <div className="w-64 ml-8 hidden md:block bg-yellow" />
          </div>
          <ServiceList
            className="flex justify-center flex-wrap mt-8"
            color="light"
          />
        </div>
      </Section>
      <HomeSection
        id="service-mail"
        background="dark"
        highlight="yellow"
        direction="down"
      >
        <article className="mx-8 p-2 flex-1">
          <Title level={2} underline="dark-green">
            Vos courriers électroniques
          </Title>
          <p>
            Famille, amis, relations professionnelles, administrations… Le
            courrier électronique a pris de nos jours une place importante et
            est probablement le service sur Internet utilisé par le plus grand
            nombre. Nous vous fournissons une boîte pour tous vos échanges. 15
            Go d’espace de stockage sont inclus.
          </p>
        </article>
        <div className="lg:w-64 w-48 hidden md:block" />
      </HomeSection>
      <HomeSection
        id="service-contacts"
        background="dark-green"
        highlight="light"
        direction="up"
      >
        <article className="mx-8 p-2 flex-1">
          <Title level={2} underline="dark">
            Vos contacts
          </Title>
          <p>
            Le courrier n’a pas de sens sans destinataire. Vous disposez d’un
            carnet d’adresses que vous pourrez synchroniser entre vos appareils.
          </p>
        </article>
        <div className="lg:w-64 w-48 hidden md:block" />
      </HomeSection>
      <HomeSection
        id="service-calendar"
        background="light"
        highlight="yellow"
        direction="down"
      >
        <article className="mx-8 p-2 flex-1">
          <Title level={2} underline="yellow">
            Votre agenda
          </Title>
          <p>Ne manquez aucun rendez-vous grâce à votre agenda.</p>
          <p>
            Besoin de séparer le professionnel et le privé ? Créez un agenda
            supplémentaire et n’affichez que celui qui est pertinent sur le
            moment.
          </p>
        </article>
        <div className="lg:w-64 w-48 hidden md:block" />
      </HomeSection>
      <HomeSection
        id="service-chat"
        background="yellow"
        highlight="dark"
        direction="up"
      >
        <article className="mx-8 p-2 flex-1">
          <Title level={2} underline="light">
            Une messagerie sécurisée pour toutes vos discussions
          </Title>
          <p>
            Pour toutes ces situations ou la conversation doit être plus animée
            que ne le permettraient le courrier électronique et sa nature
            asynchrone, notre messagerie instantanée est à votre disposition.
            Retrouvez y vos discussions directes avec vos contacts, mais aussi
            vos salons pour parler à plusieurs. Organisez vos prochaines
            sorties, collaborez sur vos projets, depuis votre téléphone ou sur
            votre ordinateur.
          </p>
          <p>
            Parce que ces échanges ne regardent que leurs participants, le
            chiffrement de bout en bout est disponible. Les clés sont détenues
            par l’utilisateur et ni Tenoco ni aucun autre tiers n’y a accès.
          </p>
          <p>
            Et comme parfois le texte n’est pas suffisant, les appels vocaux et
            visio sont intégrés à cette messagerie.
          </p>
        </article>
        <div className="lg:w-64 w-48 hidden md:block" />
      </HomeSection>
      <HomeSection
        id="service-social"
        background="dark"
        highlight="dark-green"
        direction="down"
      >
        <article className="mx-8 p-2 flex-1">
          <Title level={2} underline="dark-green">
            Un flux social pour s’ouvrir au monde
          </Title>
          <p>
            Partagez des informations. Annoncez vos évènements importants.
            Rejoignez une place publique sur laquelle participer à des débats.
          </p>
        </article>
        <div className="lg:w-64 w-48 hidden md:block" />
      </HomeSection>
      <section className="bg-light">
        <div className="sawtooth-separation" />
        <Form ref={formRef} onError={onError} onSubmit={onSubmit} />
      </section>
    </>
  );
};

export default Home;
