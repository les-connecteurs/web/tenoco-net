import React from 'react';
import type { TFunction } from 'i18next';
import { useTranslation } from 'react-i18next';

import type { Color } from '../../components/colors';
import Service from '../../components/ui/service';

export type ServiceAnchor =
  | 'service-chat'
  | 'service-mail'
  | 'service-contacts'
  | 'service-calendar'
  | 'service-social';

type ServiceDefinition = {
  anchor: ServiceAnchor;
  icon: string;
  title: string;
  subtitle: string;
};

const services = (t: TFunction): ServiceDefinition[] => [
  {
    anchor: 'service-mail',
    icon: 'fa-envelope-open',
    title: t('services.mail.title'),
    subtitle: t('services.mail.subtitle'),
  },
  {
    anchor: 'service-contacts',
    icon: 'fa-address-book',
    title: t('services.contacts.title'),
    subtitle: t('services.contacts.subtitle'),
  },
  {
    anchor: 'service-calendar',
    icon: 'fa-calendar',
    title: t('services.calendar.title'),
    subtitle: t('services.calendar.subtitle'),
  },
  {
    anchor: 'service-chat',
    icon: 'fa-matrix-org',
    title: t('services.chat.title'),
    subtitle: t('services.chat.subtitle'),
  },
  {
    anchor: 'service-social',
    icon: 'fa-mastodon',
    title: t('services.social.title'),
    subtitle: t('services.social.subtitle'),
  },
];

type ServiceListProps = {
  color: Color;
  active?: ServiceAnchor;
} & React.HTMLProps<HTMLUListElement>;

const ServiceList: React.FC<ServiceListProps> = ({
  color,
  active,
  ...props
}: ServiceListProps) => (
  <ul {...props}>
    {services(useTranslation().t).map(
      ({ anchor, icon, title, subtitle }, index) => (
        <li key={index}>
          <Service
            active={active === anchor}
            icon={icon}
            title={title}
            subtitle={subtitle}
            href={`#${anchor}`}
            color={color}
          />
        </li>
      ),
    )}
  </ul>
);

export default ServiceList;
