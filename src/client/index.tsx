/// <reference types="react-dom/experimental" />
/// <reference types="react/experimental" />

import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';

import type { ClientData } from '../data';
import App from '../App';
import { ErrorManager } from '../error-manager';
import i18n from './i18n';

declare global {
  interface Window {
    __CLIENT_DATA__?: ClientData;
    __REACT_ROOT__?: ReactDOM.Root;
  }
}

let hydrate = false;

if (window.__CLIENT_DATA__) {
  hydrate = true;
  const res = window.__CLIENT_DATA__.i18n;
  for (const lng in res) {
    for (const ns in res[lng]) {
      i18n.addResourceBundle(lng, ns, res[lng][ns], true, true);
    }
  }
}

if (!window.__REACT_ROOT__) {
  const root = document.getElementById('root');
  if (!root) {
    throw new Error('No root element');
  }

  // Save the react root for later HMR
  window.__REACT_ROOT__ = ReactDOM.unstable_createBlockingRoot(root, {
    hydrate,
  });
}

window.__REACT_ROOT__.render(
  <React.StrictMode>
    <I18nextProvider i18n={i18n}>
      <React.Suspense fallback="Loading…">
        <ErrorManager
          defaultErrors={
            window.__CLIENT_DATA__?.errors || [{ message: 'Not in SSR mode' }]
          }
        >
          <App />
        </ErrorManager>
      </React.Suspense>
    </I18nextProvider>
  </React.StrictMode>,
);

if (import.meta.hot) {
  import.meta.hot.accept();
}
