import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

import Backend from 'i18next-fetch-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

const i18n = i18next.createInstance();

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    lng: 'fr',
    fallbackLng: 'fr',
    supportedLngs: ['fr', 'en'],
    defaultNS: 'translation',
    detection: {
      // Order detection as follow:
      //  - querystring for explicit language setting
      //  - cookie, to persist the detection and have it coherent with SSR
      //  - htmlTag, to get the one detected by the SSR
      //  - navigator, to fallback to the browser lang
      // TODO: temporarily disable cookies because of CORS issues
      order: ['querystring', /* 'cookie', */ 'htmlTag', 'navigator'],
      caches: [
        /* 'cookie' */
      ],
    },
  });

export default i18n;
