import React from 'react';

import Home from './pages/home';
import Notifications from './components/layout/notifications';

const App: React.FC = () => (
  <>
    <Home />
    <Notifications />
  </>
);

export default App;
