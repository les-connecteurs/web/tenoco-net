module.exports = {
  async up(knex) {
    await knex.schema.createTable('subscriptions', (table) => {
      table.increments('id');
      table.string('email').unique().notNullable();
      table.string('user_agent');
      table.string('ip');
      table.string('lang');
      table.timestamps(false, true);
    });
  },

  async down(knex) {
    await knex.schema.dropTable('subscriptions');
  },
};
