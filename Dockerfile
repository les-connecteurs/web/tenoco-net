FROM node:lts-alpine AS builder

WORKDIR /app

# install dependencies
COPY package.json package-lock.json ./
RUN npm ci

# copy source files and build the app
COPY . .
RUN npm run build


FROM node:lts-alpine

WORKDIR /app
EXPOSE 3042
ENV NODE_ENV=production

COPY package.json package-lock.json ./
RUN npm ci

COPY --from=builder /app/lib ./lib
COPY --from=builder /app/build ./build
COPY locales ./locales
COPY migrations ./migrations
COPY knexfile.js .

CMD [ "npm", "run", "start:server" ]
