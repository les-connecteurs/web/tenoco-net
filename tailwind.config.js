module.exports = {
  purge: { enabled: false },
  theme: {
    fontFamily: {
      display: ['"Roboto Slab"', 'serif'],
      body: [
        '"Source Sans Pro"',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        'Oxygen-Sans',
        'Ubuntu',
        'Cantarell',
        '"Helvetica Neue"',
        'sans-serif',
      ],
    },

    extend: {
      colors: {
        'dark-green': '#1A535C',
        red: '#FF6B6B',
        teal: '#4ECDC4',
        yellow: '#FFE66D',
        dark: '#0A1F22',
        light: '#F7FFF7',
      },

      minHeight: {
        'most-screen': '70vh',
        'half-screen': '50vh',
      },

      spacing: {
        '80': '20rem',
        '96': '24rem',
      },

      zIndex: {
        '-1': '-1',
        '-2': '-2',
      },
    },
  },
  variants: {},
  plugins: [],
};
