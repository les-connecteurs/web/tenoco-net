module.exports = {
  extends: '@snowpack/app-scripts-react',
  scripts: {
    'build:css': 'postcss',
    'build:css::watch': '$1 --watch',
    'run:lint': "eslint 'src/**/*.{js,jsx,ts,tsx}'",
    'run:lint::watch': 'watch "$1" src',
    'run:tsc-node': 'tsc -p tsconfig.node.json',
    'run:tsc-node::watch': '$1 --watch',
    'mount:locales': 'mount locales --to /locales',
    'mount:fonts': 'mount node_modules/fork-awesome/fonts --to /fonts',
  },
  plugins: [],
  exclude: [
    '**/node_modules/**/*',
    '**/__tests__/*',
    '**/*.@(spec|test).@(js|mjs)',
    'server/**/*',
  ],
  installOptions: {
    rollup: {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      plugins: [require('rollup-plugin-terser').terser()],
    },
  },
};
